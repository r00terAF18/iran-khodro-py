try:
    import os
    import urllib.request
    import cv2

    import requests
    from bs4 import BeautifulSoup as bs4
    from selenium import webdriver
    from selenium.webdriver.chrome.options import Options
    from selenium.webdriver.support.ui import WebDriverWait

    import random
    import datetime
    import time

except ImportError or ImportWarning:
    print("[!] There were some errors/warnings with imports")
    exitNow = input('Do you want to exit and check requirements? (y/n) ')
    if 'y' in exitNow or 'Y' in exitNow:
        exit()
    else:
        pass
finally:
    pass

try:
    class WebDriver:
        def __init__(self):
            self.webdriverWait = WebDriverWait
            self.options = Options()
            self.options.add_argument('--headless')
            self.options.add_argument('--disable-gpu')
            # self.options.add_argument('--no-log')
            self.driver = webdriver.Chrome('chromedriver', options=self.options)
            print("[+] Started a chromewebdriver session")

        def go(self, url):
            self.driver.get(url)
            self.waitFor(5)

        def waitFor(self, sec):
            self.driver.implicitly_wait(sec)

        def find_ID(self, id):
            if 'btn' in id or 'Button' in id or 'button' in id:
                self.driver.find_element_by_id(id).click()
            else:
                return self.driver.find_element_by_id(id)

        def find_css(self, css):
            return self.driver.find_element_by_css_selector(css)

        def find_class(self, class_name):
            return self.driver.find_element_by_class_name(class_name)

        def find_tag(self, tag):
            return self.driver.find_element_by_tag_name(tag)

        def find_name(self, name):
            return self.driver.find_element_by_name(name)

        def download(self, url, name):
            urllib.request.urlretrieve(url, name)

        def enter_ID(self, id, data):
            self.driver.find_element_by_id(id).send_keys(data)

        def enter_css(self, css, data):
            self.driver.find_element_by_css_selector(css).send_keys(data)

        def enter_class(self, class_name, data):
            self.driver.find_element_by_class_name(class_name).send_keys(data)

        def enter_name(self, name, data):
            self.driver.find_element_by_name(name).send_keys(data)

        def click_button_ID(self, id):
            self.driver.find_element_by_id(id).click()

        def stop(self):
            self.driver.stop_client()
            self.driver.close()
            self.driver.quit()
except EnvironmentError or AttributeError:
    print("[-] There was an Error initializing the WebDriver Class!")
    exitNow = input('Do you want to exit and check requirements? (y/n) ')
    if 'y' in exitNow or 'Y' in exitNow:
        exit()
    else:
        pass
finally:
    pass

def draw_logo():
    logo = '''
                    --------------------------------------------------------------------------------------------
                    |                                                                                          |
                    |                 AAAA               AA                   AAAA                             |
                    |              AAAA                  AA                  AA  AA                            |
                    |   AAAAAAAA    AAAA       AAAAAAAA  AA  AAAAAAAA       AA    AA       AAAAAAAA  AAAAAAAA  |
                    |   A      A     AAA              A  AA  A      A      AA      AA      A      A  A      A  |
                    |   AAAAAAAA       AAAA           A  AA  AAAAAAAA     AA        AA     A      A  A      A  |
                    |   A               AAAA   AAAAAAAA  AA  A           AAAAAAAAAAAAAA    AAAAAAAA  AAAAAAAA  |
                    |   A            AAAA      A      A  AA  A          AA            AA   A         A         |
                    |   AAAAAAA   AAAA         AAAAAAAA  AA  AAAAAAAA  AA              AA  A         A         |
                    --------------------------------------------------------------------------------------------
    \n
    '''
    print(logo)


class eSaleApp():
    def __init__(self):
        self.cls()
        draw_logo()
        self.got_credentials = False
        self.loged_in = False
        self.driver = WebDriver()
        self.driver.go('https://esale.ikco.ir/#!/Login?t=0')  # head over to the login page already at start
        self.getID()
        self.tabs = ['https://esale.ikco.ir/#!/',
                     'https://esale.ikco.ir/#!/customer/dashboard',
                     'https://esale.ikco.ir/#!/searchcars?cca=0&csb=1&ccc=0&cpd=0',
                     'https://esale.ikco.ir/#!/searchcars?cca=0&csb=2&ccc=0&cpd=0',
                     'https://esale.ikco.ir/#!/searchcars?cca=0&csb=3&ccc=0&cpd=0',
                     'https://esale.ikco.ir/#!/customer/customerProfile']


    def cls(self):
        os.system('cls' if os.name == 'nt' else 'clear')

    def exit(self):
        try:
            os.remove('cap.png')
            self.driver.stop()
            self.cls()
            draw_logo()
        except FileNotFoundError or AttributeError:
            self.cls()
            draw_logo()
        finally:
            pass

    def getID(self):
        enteringCredentials = True
        enteringUsrName = True
        enteringPasswd = True
        enteringTime = True
        while enteringCredentials:
            while enteringUsrName:
                self.usrname = input("[+] Username: ")
                if self.usrname == "" or self.usrname == " ":
                    print("[-] Invalid input")
                else:
                    enteringUsrName = False

            while enteringPasswd:
                self.passwd = input("[+] Password: ")
                if self.passwd == "" or self.passwd == " ":
                    print("[-] Invalid input")
                else:
                    enteringPasswd = False

            while enteringTime:
                try:
                    self.time_to_buy = int(input("[+] Time to start purchase(HH): "))
                    if self.time_to_buy == "" or self.time_to_buy == " ":
                        print("[-] Invalid input")
                    else:
                        enteringTime = False
                except ValueError:
                    print("[-] Invalid input")
                finally:
                    pass
            enteringCredentials = False

        print(f'[+] Got following credentials Username: {self.usrname}, and Password: {self.passwd}')
        rightID = input("Is that right? (y/n) ")
        if "y" in rightID or "Y" in rightID:
            self.cls()
            self.got_credentials = True
        else:
            self.getID()

    def chooseCar(self):
        carcode = input("Enter Car Code: ")
        my_div = ''

        request = requests.get("https://esale.ikco.ir/#!/searchcars?cca=0&csb=0&ccc=0&cpd=0")
        print("[+] requested website content")
        result = request.content
        print("[+] Aquired website content and saved it")
        soup = bs4(result, 'lxml')
        print("[+] encoding website content")

        divsAll = soup.find_all("span", attrs={"class": "col-lg-7 col-md-7 col-sm-7 col-xs-7 ng-binding"})
        print("[+] filtered spans")

        for div in divsAll:
            if carcode in div.string:
                my_div = div.find_parent('div')

        btn = my_div.find_parent('div')
        btn = btn.find_parent('div')
        btn = btn.find('button')
        print("[+] found purchase button")
        self.driver.click_button_ID(btn['id'])

    def login(self):
        if not self.got_credentials:
            print("[!] You have loged out, credentials must be re-entered")
            self.getID()

        self.driver.enter_name('userNameNew', self.usrname)
        self.driver.waitFor(1)
        self.driver.enter_name('passwordNew', self.passwd)
        self.driver.find_ID('LoginButton')
        self.loged_in = True

    def stay_loged_in(self):
        if not self.got_credentials:  # later must be changed to self.loged_in
            print("[!] You have not logged in, moving you over")
            self.getID()  # later must be changed to self.login()
        else:
            while datetime.datetime.now().hour < self.time_to_buy:
                print(f'[+] Current time is {datetime.datetime.now().hour} and entered time is {self.time_to_buy}')
                x = random.randrange(0, len(self.tabs))
                self.driver.go(self.tabs[x])
                print(f'[+] Changed to tab number {x}')
                if datetime.datetime.now().hour < self.time_to_buy:
                    time.sleep(45)
                else:
                    pass

    def logout(self):
        self.loged_in = False

    def show_captcha(self):
        cap = self.driver.find_tag('img')
        for src in cap:
            if len(src.get_attribute('src')) > 1000:
                self.imgLink = src.get_attribute('src')

        urllib.request.urlretrieve(self.imgLink, 'cap.png')
        img = cv2.imread('cap.jpg')
        cv2.imshow('Captcha', img)
        cv2.waitKey(0)

try:
    app = eSaleApp()

    waiting = True
    while waiting:
        app.cls()
        draw_logo()
        print("[1] Login")
        print("[2] Stay logged in")
        print("[3] Purchase Car")
        print("[4] Logout")
        print("[5] Exit")
        usrInput = input(">>> ")
        if usrInput == "1":
            app.cls()
            app.login()
        elif usrInput == "2":
            app.cls()
            app.stay_loged_in()
        elif usrInput == "3":
            app.cls()
            app.chooseCar()
        elif usrInput == "4":
            app.cls()
            app.logout()
        elif usrInput == "5":
            app.cls()
            app.exit()
            waiting = False

except KeyboardInterrupt:
    os.system('cls' if os.name == 'nt' else 'clear')
    draw_logo()
    #print("\n[-] User interrupted programm!")
finally:
    pass
