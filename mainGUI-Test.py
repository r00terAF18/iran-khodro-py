import os
import sys
import urllib.request
import cv2

import requests
from bs4 import BeautifulSoup as bs4
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait

import random
import datetime
import time


from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import QSize, QRect, Qt
from PyQt5.QtGui import QFont, QPixmap
from PyQt5.QtWidgets import QLabel, QLineEdit, QCheckBox, QComboBox \
    , QMainWindow, QApplication, QPushButton, QStatusBar

try:
    class WebDriver:
        def __init__(self):
            self.webdriverWait = WebDriverWait
            self.options = Options()
            self.options.add_argument('--headless')
            self.options.add_argument('--disable-gpu')
            # self.options.add_argument('--no-log')
            self.driver = webdriver.Chrome('chromedriver')
            print("[+] Started a chromewebdriver session")

        def go(self, url):
            self.driver.get(url)
            self.waitFor(5)

        def waitFor(self, sec):
            self.driver.implicitly_wait(sec)

        def find_ID(self, id):
            if 'btn' in id or 'Button' in id or 'button' in id:
                self.driver.find_element_by_id(id).click()
            else:
                return self.driver.find_element_by_id(id)

        def find_css(self, css):
            return self.driver.find_element_by_css_selector(css)

        def find_class(self, class_name):
            return self.driver.find_element_by_class_name(class_name)

        def find_tag(self, tag):
            return self.driver.find_element_by_tag_name(tag)

        def find_name(self, name):
            return self.driver.find_element_by_name(name)

        def download(self, url, name):
            urllib.request.urlretrieve(url, name)

        def enter_ID(self, id, data):
            self.driver.find_element_by_id(id).send_keys(data)

        def enter_css(self, css, data):
            self.driver.find_element_by_css_selector(css).send_keys(data)

        def enter_class(self, class_name, data):
            self.driver.find_element_by_class_name(class_name).send_keys(data)

        def enter_name(self, name, data):
            self.driver.find_element_by_name(name).send_keys(data)

        def click_button_ID(self, id):
            self.driver.find_element_by_id(id).click()

        def stop(self):
            self.driver.stop_client()
            self.driver.close()
            self.driver.quit()
except EnvironmentError or AttributeError:
    print("[-] There was an Error initializing the WebDriver Class!")
finally:
    pass


class MainApp(QMainWindow, WebDriver):

    def __init__(self):
        super().__init__()
        self.initUI()
        self.imgLink = ''
        self.isLogedIn = False
        self.currentTime = ''
        self.tabs = ['https://esale.ikco.ir/#!/',
                     'https://esale.ikco.ir/#!/customer/dashboard',
                     'https://esale.ikco.ir/#!/searchcars?cca=0&csb=1&ccc=0&cpd=0',
                     'https://esale.ikco.ir/#!/searchcars?cca=0&csb=2&ccc=0&cpd=0',
                     'https://esale.ikco.ir/#!/searchcars?cca=0&csb=3&ccc=0&cpd=0',
                     'https://esale.ikco.ir/#!/customer/customerProfile']
        self.UIHandler()
        self.ButtonHandler()

    def initUI(self):
        self.resize(450, 350) # 368, 234
        self.setMaximumSize(QSize(999, 999))
        self.setWindowTitle("Iran Khodro")

        font = QFont()
        font.setFamily("Trebuchet MS")
        font.setPointSize(12)

        self.setFont(font)

        self.lblTime = QLabel(self)
        self.lblTime.setGeometry(QRect(280, 10, 91, 20))
        self.lblTime.setObjectName("lblTime")

        lblUsrName = QLabel(self)
        lblUsrName.setGeometry(QRect(10, 10, 71, 16))
        lblUsrName.setText("Username")

        lblPasswd = QLabel(self)
        lblPasswd.setGeometry(QRect(10, 35, 71, 16))
        lblPasswd.setText("Password")

        self.txtUsrName = QLineEdit(self)
        self.txtUsrName.setGeometry(QRect(90, 10, 130, 22))
        self.txtUsrName.setObjectName("txtUsrName")
        self.txtUsrName.setText("1861600976")

        self.txtPasswd = QLineEdit(self)
        self.txtPasswd.setGeometry(QRect(90, 35, 130, 22))
        self.txtPasswd.setObjectName("txtPasswd")
        self.txtPasswd.setText("A1234567")

        lblPrefCar = QLabel(self)
        lblPrefCar.setGeometry(QRect(10, 60, 101, 16))
        lblPrefCar.setObjectName("lblPrefCar")
        lblPrefCar.setText("Car Code")

        self.txtCarCode = QLineEdit(self)
        self.txtCarCode.setGeometry(QRect(90, 60, 130, 22))
        self.txtCarCode.setObjectName("txtCarCode")
        self.txtCarCode.setText("96810")

        self.startTime = QComboBox(self)
        self.startTime.setGeometry(QRect(230, 10, 41, 22))
        self.startTime.setObjectName("startTime")
        times = ["10", "11"]
        for item in times:
            self.startTime.addItem(item)

        self.checkBoxShowGUI = QCheckBox(self)
        self.checkBoxShowGUI.setText("Show\nChrome")
        self.checkBoxShowGUI.setGeometry(QRect(230, 40, 91, 16))
        self.checkBoxShowGUI.setObjectName("checkBoxShowGUI")

        self.btnLogin = QPushButton(self)
        self.btnLogin.setGeometry(QRect(230, 60, 61, 23))
        self.btnLogin.setObjectName("btnLogin")
        self.btnLogin.setText("Login")

        self.btnStartOrder = QPushButton(self)
        self.btnStartOrder.setGeometry(QRect(230, 190, 91, 23))
        self.btnStartOrder.setObjectName("btnStartOrder")
        self.btnStartOrder.setText("Start Order")

        self.txtCap = QLineEdit(self)
        self.txtCap.setGeometry(QRect(10, 190, 131, 20))
        self.txtCap.setObjectName("txtCap")

        self.btnSend = QPushButton(self)
        self.btnSend.setGeometry(QRect(150, 190, 61, 23))
        self.btnSend.setObjectName("btnSend")
        self.btnSend.setText("Send")

        self.lblImg = QLabel(self)
        self.lblImg.setGeometry(QRect(10, 90, 150, 55))
        self.lblImg.setAlignment(Qt.AlignCenter)
        self.lblImg.setObjectName("lblImg")

        self.show()

    def ButtonHandler(self):
        self.btnStartOrder.clicked.connect(self.startPurchase)
        self.btnLogin.clicked.connect(self.login)

    def UIHandler(self):
        # the purpose of this try and catch block, is to show the time from a server pool
        # but it wont work if you dont have an internet connection
        '''
        try:
            ntp_client = ntplib.NTPClient()
            response = ntp_client.request('pool.ntp.org')
            time_format = "%H:%M"
            self.currentTime = time.strftime(time_format, time.localtime(response.tx_time))
            self.lblTime.setText(self.currentTime)
        except TimeoutError:
            print('Could not sync with time server.')
        '''


    def login(self):
        if not self.checkBoxShowGUI.isChecked():
            self.driver = WebDriver(showchrome=False)
        else:
            self.driver = WebDriver(showchrome=True)

        self.driver.go('https://esale.ikco.ir/#!/Login?t=0')

        self.driver.enter_name('userNameNew', self.txtUsrName.text())

        self.driver.waitFor(1)
        # time.sleep(2)

        self.driver.enter_name('passwordNew', self.txtPasswd.text())

        self.driver.find_ID('LoginButton')
        self.isLogedIn = True


    def startPurchase(self):
        carCode = self.txtCarCode.text()
        spanList = []

        requst = requests.get("http://localhost:8080/dashboard/")
        result = requst.content
        soup = bs4(result, 'lxml')

        # divsAll = soup.findAll("div", attrs={"ng-repeat": "searchCar in vm.Cars"})
        # divsAll = soup.find_all("span", attrs={"class": "col-lg-7 col-md-7 col-sm-7 col-xs-7 ng-binding"})
        #
        # for contents in divsAll:
        #     if carCode in contents.string:
        #         print(contents)
        #     else:
        #         pass


        ### Stackoverflow solution ###

        # my_file = soup.find_all("div", attrs={"ng-repeat": "searchCar in vm.Cars"})
        # for div in my_file['div']:
        #     for span in div.find_all('span'):
        #         if span.text == carCode:
        #             my_file['Abstract'] = div
        #             print(div)


        ###### print(soup.find_all("span", attrs={"class": "col-lg-7 col-md-7 col-sm-7 col-xs-7 ng-binding"}))
        #
        # cap = self.driver.find_tag('img')
        # for src in cap:
        #     if len(src.get_attribute('src')) > 1000:
        #         self.imgLink = src.get_attribute('src')
        #
        # time.sleep(1)
        #
        # self.lblImg.setPixmap(QPixmap('cap.png', '1'))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainApp = MainApp()
    sys.exit(app.exec_())
