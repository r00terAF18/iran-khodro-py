import os
import urllib.request
import cv2

import requests
from bs4 import BeautifulSoup as bs4
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait


class WebDriver:
    def __init__(self):
        self.webdriverWait = WebDriverWait
        self.options = Options()
        self.options.add_argument('--headless')
        self.options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome('chromedriver', options=self.options)
        print("[+] Started a chromewebdriver session")

    def go(self, url):
        self.driver.get(url)
        self.waitFor(5)

    def waitFor(self, sec):
        self.driver.implicitly_wait(sec)

    def find_ID(self, id):
        if 'btn' in id or 'Button' in id or 'button' in id:
            self.driver.find_element_by_id(id).click()
        else:
            return self.driver.find_element_by_id(id)

    def find_css(self, css):
        return self.driver.find_element_by_css_selector(css)

    def find_class(self, class_name):
        return self.driver.find_element_by_class_name(class_name)

    def find_tag(self, tag):
        return self.driver.find_element_by_tag_name(tag)

    def find_name(self, name):
        return self.driver.find_element_by_name(name)

    def download(self, url, name):
        urllib.request.urlretrieve(url, name)

    def enter_ID(self, id, data):
        self.driver.find_element_by_id(id).send_keys(data)

    def enter_css(self, css, data):
        self.driver.find_element_by_css_selector(css).send_keys(data)

    def enter_class(self, class_name, data):
        self.driver.find_element_by_class_name(class_name).send_keys(data)

    def enter_name(self, name, data):
        self.driver.find_element_by_name(name).send_keys(data)

    def click_button_ID(self, id):
        self.driver.find_element_by_id(id).click()

    def stop(self):
        self.driver.stop_client()
        self.driver.close()
        self.driver.quit()


class eSaleApp():
    def __init__(self):
        self.logedin = False
        # self.driver = WebDriver()
        self.getID()

    def cls(self):
        os.system('cls' if os.name == 'nt' else 'clear')

    def exit(self):
        # os.remove('cap.png')
        self.driver.stop()
        print("[!] Application ended")

    def getID(self):
        enteringCredentials = True
        enteringUsrName = True
        enteringPasswd = True
        while enteringCredentials:
            while enteringUsrName:
                self.usrname = input("[+] Username: ")
                if self.usrname == "" or self.usrname == " ":
                    print("[-] Invalid input")
                else:
                    enteringUsrName = False

            while enteringPasswd:
                self.passwd = input("[+] Password: ")
                if self.passwd == "" or self.passwd == " ":
                    print("[-] Invalid input")
                else:
                    enteringPasswd = False
            enteringCredentials = False

        print(f'[+] Got following credentials Username={self.usrname}, and Password={self.passwd}')
        rightID = input("Is that right? ")
        if "y" in rightID or "Y" in rightID:
            self.cls()
        else:
            self.getID()

    def chooseCar(self):
        carcode = input("Enter Car Code: ")
        my_div = ''

        request = requests.get("https://esale.ikco.ir/#!/searchcars?cca=0&csb=0&ccc=0&cpd=0")
        print("[+] requested website content")
        result = request.content
        print("[+] Aquired website content and saved it")
        soup = bs4(result, 'lxml')
        print("[+] encoding website content")

        divsAll = soup.find_all("span", attrs={"class": "col-lg-7 col-md-7 col-sm-7 col-xs-7 ng-binding"})
        print("[+] filtered spans")

        for div in divsAll:
            if carcode in div.string:
                my_div = div.find_parent('div')

        btn = my_div.find_parent('div')
        btn = btn.find_parent('div')
        btn = btn.find('button')
        print("[+] found purchase button")
        self.driver.click_button_ID(btn['id'])

    def login(self):
        if not self.logedin:
            print("[!] You have loged out, credentials must be re-entered")
            self.getID()
        self.driver.go('https://esale.ikco.ir/#!/Login?t=0')
        self.driver.enter_name('userNameNew', self.usrname)
        self.driver.waitFor(1)
        self.driver.enter_name('passwordNew', self.passwd)
        self.driver.find_ID('LoginButton')
        self.logedin = True

    def logout(self):
        self.logedin = False

    def show_captcha(self):
        # cap = self.driver.find_tag('img')
        # for src in cap:
        #     if len(src.get_attribute('src')) > 1000:
        #         self.imgLink = src.get_attribute('src')
        #
        # urllib.request.urlretrieve(self.imgLink, 'cap.png')
        img = cv2.imread('cap.jpg')
        cv2.imshow('Captcha', img)
        cv2.waitKey(0)


app = eSaleApp()

waiting = True
while waiting:
    app.cls()
    print("[1] Login")
    print("[2] Purchase Car")
    print("[3] Logout")
    print("[4] Exit")
    usrInput = input(">>> ")
    if usrInput == "1":
        app.cls()
        app.login()
    elif usrInput == "2":
        app.cls()
        app.chooseCar()
    elif usrInput == "3":
        app.cls()
        app.logout()
    elif usrInput == "4":
        app.cls()
        app.exit()
        waiting = False


# driver.get('https://esale.ikco.ir/#!/searchcars?cca=0&csb=0&ccc=0&cpd=0')
# print('went to purchase screen')


# driver.find_element_by_css_selector("[ng-mousedown=\"regenerateCaptcha()\"]").click()
# print('regenareted captcha')
# time.sleep(2)
#
# cap = driver.find_elements_by_tag_name('img')
# for src in cap:
#     if len(src.get_attribute('src')) > 1000:
#         imgLink = src.get_attribute('src')
# print('retrieved captcha')
#
# urllib.request.urlretrieve(imgLink, 'cap.png')
# print('downloaded captcha image')
#
# cap = self.driver.find_tag('img')
# for src in cap:
#     if len(src.get_attribute('src')) > 1000:
#         self.imgLink = src.get_attribute('src')
#
# time.sleep(1)
#
# self.lblImg.setPixmap(QPixmap('cap.png', '1'))

