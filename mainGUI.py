import os
import sys
import time
import ntplib
import random
import urllib.request

from PyQt5 import QtCore
from PyQt5.QtCore import QSize, QRect, Qt
from PyQt5.QtGui import QFont, QPixmap
from PyQt5.QtWidgets import QLabel, QLineEdit, QCheckBox, QComboBox \
    ,QMainWindow, QApplication, QPushButton

from selenium import webdriver
from selenium.webdriver import chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support.expected_conditions import \
    presence_of_element_located as EC
from selenium.common.exceptions import TimeoutException


class MainApp(QMainWindow):
    def __init__(self):
        self.initUI()
        self.UIHandler()
        self.ButtonHandler()
        self.options = Options()
        self.options.add_argument('--headless')
        self.options.add_argument('--disable-gpu')
        self.driver = webdriver
        self.imgLink = ''
        self.seenCars = False
        self.tabs = ['https://esale.ikco.ir/#!/',
                     'https://esale.ikco.ir/#!/customer/dashboard',
                     'https://esale.ikco.ir/#!/searchcars?cca=0&csb=1&ccc=0&cpd=0',
                     'https://esale.ikco.ir/#!/searchcars?cca=0&csb=2&ccc=0&cpd=0',
                     'https://esale.ikco.ir/#!/searchcars?cca=0&csb=3&ccc=0&cpd=0',
                     'https://esale.ikco.ir/#!/customer/customerProfile']

    def initUI(self):
        self.resize(480, 290)
        self.setMaximumSize(QSize(999, 999))
        self.setWindowTitle("Iran Khodro")

        font = QFont()
        font.setFamily("Trebuchet MS")
        font.setPointSize(12)

        self.setFont(font)

        self.lblTime = QLabel(self)
        self.lblTime.setGeometry(QRect(370, 10, 90, 20))
        self.lblTime.setObjectName("lblTime")

        lblUsrName = QLabel(self)
        lblUsrName.setGeometry(QRect(10, 10, 71, 16))
        lblUsrName.setText("Username")

        lblPasswd = QLabel(self)
        lblPasswd.setGeometry(QRect(10, 30, 71, 16))
        lblPasswd.setText("Password")

        self.txtUsrName = QLineEdit(self)
        self.txtUsrName.setGeometry(QRect(90, 10, 131, 20))
        self.txtUsrName.setObjectName("txtUsrName")
        self.txtUsrName.setText("1861600976")

        self.txtPasswd = QLineEdit(self)
        self.txtPasswd.setGeometry(QRect(90, 30, 131, 20))
        self.txtPasswd.setObjectName("txtPasswd")
        self.txtPasswd.setText("A1234567")

        lblPrefCar = QLabel(self)
        lblPrefCar.setGeometry(QRect(10, 60, 101, 16))
        lblPrefCar.setObjectName("lblPrefCar")
        lblPrefCar.setText("Prefered Car")

        self.prfOrder = QComboBox(self)
        self.prfOrder.setGeometry(QRect(110, 60, 111, 22))
        self.prfOrder.setObjectName("prfOrder")
        orders = ["Naghdi", "Pish", "Etebari"]
        for item in orders:
            self.prfOrder.addItem(item)

        self.startTime = QComboBox(self)
        self.startTime.setGeometry(QRect(310, 10, 41, 22))
        self.startTime.setObjectName("startTime")
        times = ["10", "11"]
        for item in times:
            self.startTime.addItem(item)

        self.checkBoxShowGUI = QCheckBox(self)
        self.checkBoxShowGUI.setText("Show\nChrome")
        self.checkBoxShowGUI.setGeometry(QRect(230, 40, 81, 41))
        self.checkBoxShowGUI.setObjectName("checkBoxShowGUI")

        self.btnStart = QPushButton(self)
        self.btnStart.setGeometry(QRect(240, 10, 61, 23))
        self.btnStart.setObjectName("btnLogin")
        self.btnStart.setText("Start")

        self.txtCap = QLineEdit(self)
        self.txtCap.setGeometry(QRect(10, 190, 131, 20))
        self.txtCap.setObjectName("txtCap")

        self.btnSend = QPushButton(self)
        self.btnSend.setGeometry(QRect(150, 190, 61, 23))
        self.btnSend.setObjectName("btnSend")
        self.btnSend.setText("Send")

        self.lblImg = QLabel(self)
        self.lblImg.setGeometry(QRect(10, 90, 150, 55))
        self.lblImg.setAlignment(Qt.AlignCenter)
        self.lblImg.setObjectName("lblImg")

        self.show()

    def ButtonHandler(self):
        self.btnStart.clicked.connect(self.startPurchase)
        self.btnSend.clicked.connect(self.sendCaptcha)

    def UIHandler(self):
        try:
            ntp_client = ntplib.NTPClient()
            response = ntp_client.request('pool.ntp.org')
            time_format = "%H:%M"
            x = time.strftime(time_format, time.localtime(response.tx_time))
            self.lblTime.setText(x)
        except:
            print('Could not sync with time server.')

    def preventLogout(self):
        if not self.checkBoxShowGUI.isChecked():
            self.driver = webdriver.Chrome('chromedriver', chrome_options=self.options)
        else:
            self.driver = webdriver.Chrome('chromedriver')

        self.driver.get("http://localhost:8080/dashboard/")  # opens a chrome window and goes to the given website
        try:
            self.webdriverWait = WebDriverWait(self.driver, 3).until(EC((By.ID, 'recaptcha-accessible-status')))
        except TimeoutException:
            print('took too much time')
        while not self.seenCars:
            x = random.randrange(0, len(self.tabs)) # 2 Naghdi - 3 Pish - 4 Etebari
            if x == 2 and self.prfOrder.currentText() == "Naghdi":
                pass
            elif x == 3 and self.prfOrder.currentText() == "Pish":
                pass
            elif x == 4 and self.prfOrder.currentText() == "Etebari":
                pass
            else:
                self.driver.get(self.tabs[x])

    def startPurchase(self):
        if not self.checkBoxShowGUI.isChecked():
            options = Options()
            options.add_argument('--headless')
            options.add_argument('--disable-gpu')
            self.driver = webdriver.Chrome('chromedriver', chrome_options=options)
        else:
            self.driver = webdriver.Chrome('chromedriver')

        self.driver.get("https://esale.ikco.ir/#!/Login?t=0")  # opens a chrome window and goes to the given website
        try:
            self.webdriverWait = WebDriverWait(self.driver, 3).until(EC((By.ID, 'recaptcha-accessible-status')))
        except TimeoutException:
            print('took too much time')

        self.driver.implicitly_wait(5)  # wait a few seconds, so that the webpage doesn't feel suspecious

        inputUsr = self.driver.find_element_by_name('userNameNew')  # find the element by its name
        inputUsr.send_keys(self.txtUsrName.text())  # type the value in its place

        time.sleep(2)

        inputPass = self.driver.find_element_by_name('passwordNew')
        inputPass.send_keys(self.txtPasswd.text())

        self.driver.find_element_by_id('LoginButton').click()
        time.sleep(15)

        self.driver.get('https://esale.ikco.ir/#!/searchcars?cca=0&csb=0&ccc=0&cpd=0')

        self.driver.find_element_by_css_selector("[ng-if=\"searchCar.srn\"]").click()
        time.sleep(2)

        self.driver.find_element_by_css_selector("[ng-mousedown=\"regenerateCaptcha()\"]").click()
        time.sleep(2)

        cap = self.driver.find_elements_by_tag_name('img')
        for src in cap:
            if len(src.get_attribute('src')) > 1000:
                self.imgLink = src.get_attribute('src')

        urllib.request.urlretrieve(self.imgLink, 'cap.png')
        time.sleep(1)

        self.lblImg.setPixmap(QPixmap('cap.png', '1'))
        self.driver.service.stop()
        self.driver.quit()

    def sendCaptcha(self):
        pass

    def _quit(self):
        self.driver.service.stop()
        self.driver.quit()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainApp = MainApp()
    sys.exit(app.exec_())
